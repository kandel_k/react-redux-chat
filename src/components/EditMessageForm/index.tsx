import React, {useEffect, useState} from "react";
import PropTypes, {InferType} from 'prop-types'
import {connect} from "react-redux";
import {Button, ButtonGroup, Form, Modal} from "semantic-ui-react";
import {bindActionCreators} from "redux";
import {cancelEdit, deleteMessage, editMessage} from "../../actions/ChatActions";
import './style.scss';

const EditMessageForm: any = ({isShow, message, editMessage, cancelEdit, deleteMessage}: InferType<typeof EditMessageForm.propTypes>) => {
    const [text, setText] = useState('');

    const onChange = (event: any) => {
        setText(event.target.value);
    }

    const onCancel = () => {
        cancelEdit();
        setText(message.text);
    }

    const onEdit = () => {
        if (text.length === 0) {
            deleteMessage(message.id);
            cancelEdit();
        }
        message.text = text;
        message.editedAt = new Date();
        editMessage(message);
    }

    const handleKeyDown = (event: any) => {
        if (event.key === 'Enter') {
            onEdit();
            event.preventDefault();
        }
    }

    useEffect(() => {
        setText(message.text);
    }, [message.text])

    return (
        <Modal className={'editMessageForm'} as={Form} open={isShow} closeOnDimmerClick={false} closeOnEscape={false}>
            <Modal.Header>Edit message</Modal.Header>
            <Modal.Content>
                <textarea onKeyDown={handleKeyDown} value={text} onChange={onChange}/>
                <ButtonGroup floated={'right'}>
                    <Button onClick={onCancel}>Cancel</Button>
                    <Button onClick={onEdit} color={"blue"}>Update</Button>
                </ButtonGroup>
            </Modal.Content>
        </Modal>
    );
}

EditMessageForm.propTypes = {
    isShow: PropTypes.bool.isRequired,
    message: PropTypes.object.isRequired,
    editMessage: PropTypes.func.isRequired,
    cancelEdit: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired
};

EditMessageForm.defaultProps = {
    isShow: false,
    message: {}
}

const mapStateToProps = ({chat}: any) => ({
    isShow: chat.isShow,
    message: chat.editMessage
});

const actions = {
    cancelEdit,
    editMessage,
    deleteMessage
}

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditMessageForm);
