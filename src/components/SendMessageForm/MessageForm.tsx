import React, {useState} from "react";
import PropTypes, {InferProps} from 'prop-types';
import {Button, Form, FormGroup, TextArea} from "semantic-ui-react";
import './style.scss'
import {MessageInterface} from "../../containers/Chat";
import {connect} from "react-redux";
import {sendMessage} from "../../actions/ChatActions";
import {bindActionCreators} from "redux";

const MessageForm: any = ({sendMessage, currUser}: InferProps<typeof MessageForm.propTypes>) => {
    const [text, setText] = useState('');

    const send = () => {
        if (text.length === 0) {
            return;
        }

        let message: MessageInterface = {
            id: '' + Math.random() * 100,
            userId: currUser.userId,
            user: currUser.user,
            text: text,
            isLiked: false,
            createdAt: new Date(),
            editedAt: ''
        }

        sendMessage(message);
        setText('');
    }

    const handleChange = (event: any) => setText(event.target.value);

    const handleKeyDown = (event: KeyboardEvent) => {
        if (event.key === 'Enter') {
            send();
            event.preventDefault();
        }
    }

    return (
        <Form className={'message-form'}>
            <FormGroup>
                <Form.Field control={TextArea} placeholder={'Type your message to send'} width={12} rows={2}
                            value={text} onChange={handleChange} onKeyDown={handleKeyDown}/>
                <Button color={"blue"} size={"medium"} onClick={send}>Send</Button>
            </FormGroup>
        </Form>
    );
}

MessageForm.propTypes = {
    sendMessage: PropTypes.func.isRequired,
    currUser: PropTypes.object.isRequired
}

const mapStateToProps = ({users}: any) => ({
    currUser: users.currUser
})

const actions = {
    sendMessage
}

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageForm);
