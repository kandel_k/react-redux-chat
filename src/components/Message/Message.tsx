import React from "react";
import PropTypes, {InferType} from 'prop-types'
import {Item} from 'semantic-ui-react'
import './style.scss'
import Moment from 'react-moment'
import noAvatar from '../../resources/img/noavatar.png'

const Message: any = ({message, currUser, deleteMessage, likeMessage, showEditForm}: InferType<typeof Message.propTypes>) => {
    const isUpdatable = message.userId === currUser.userId;

    const onDelete = () => {
        deleteMessage(message.id);
    }

    const onLike = () => {
        likeMessage(message.id);
    }

    const onEdit = () => {
        showEditForm(message);
    }

    return (
            <Item className={(isUpdatable &&'align-self-right message') || 'message'}>
                {!isUpdatable
                    && <Item.Image size={"tiny"} src={(message?.avatar.length > 0) ? message.avatar : noAvatar} />}

                <Item.Content >
                    <Item.Header as='a'>{(message.user && message.user.length > 0) ? message.user : message.userId}</Item.Header>
                    <Item.Description>{message.text}</Item.Description>
                    <Item.Extra className={'user-menu'}>

                        {isUpdatable && <p className={'hidden-button'} onClick={onEdit}>Edit</p>}
                        {isUpdatable && <p className={'hidden-button'} onClick={onDelete}>Delete</p>}
                        {(!isUpdatable && <p onClick={onLike}>{!message.isLiked ? 'Like' : 'Liked'}</p>)}

                        <Moment fromNow>{message.createdAt}</Moment>
                    </Item.Extra>
                </Item.Content>
            </Item>
    );
}

Message.propTypes = {
    message: PropTypes.object.isRequired,
    currUser: PropTypes.object.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired,
    showEditForm: PropTypes.func.isRequired
}

export default Message;
