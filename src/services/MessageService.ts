import {CallWebApi} from "./ApiService";

const url: string = 'https://edikdolynskyi.github.io/react_sources/messages.json';

export const LoadMessages = async () => {
    return await CallWebApi(url).then(result => {return result});
}
