import React, {useEffect} from 'react'
import Header from "../Header";
import './style.scss'
import Main from "../Main";
import {Dimmer, Loader} from "semantic-ui-react";
import {loadData} from '../../actions/ChatActions'
import PropTypes, {InferProps} from 'prop-types'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

export interface UserInterface {
    userId: string,
    avatar: string,
    user: string
}

export interface MessageInterface {
    id: string,
    userId: string,
    user: string,
    text: string,
    isLiked: boolean,
    createdAt: Date,
    editedAt: string
}

const Chat: any = ({ loadData, isLoading }: InferProps<typeof Chat.propTypes>) => {

    useEffect(() => {
        loadData();
    }, [loadData]);

    const showLoading = () => {
        return (
            <Dimmer active>
                <Loader>Loading</Loader>
            </Dimmer>
        )
    }

    const showChat = () => {
        return (
            <div className={'container'}>
                <Header />
                <Main />
            </div>
        );
    }

    return (
        (isLoading && showLoading()) || showChat()
    );
}

Chat.defaultProps = {
    isLoading: true
}

Chat.propTypes = {
    loadData: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired
}

const mapStateToProps = ({chat}:any) => ({
    isLoading: chat.isLoading
});

const actions = {
    loadData
}

const mapDispatchToProps = (dispatch: any):any => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);
