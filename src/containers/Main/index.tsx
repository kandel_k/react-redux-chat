import React from "react";
import ChatField from "../ChatField";
import './style.scss'

const Main = () => {
    return(
        <main>
            <div className='wrapper'>
                <ChatField />
            </div>
        </main>
    )
}

export default Main;
