import React from "react";
import MessageForm from "../../components/SendMessageForm/MessageForm";
import './style.scss'
import MessageContainer from "../MessageContainer";
import EditMessageForm from "../../components/EditMessageForm";

const ChatField = () => {

    return (
        <div className={'chat-field'}>
            <MessageContainer />
            <MessageForm />
            <EditMessageForm />
        </div>
    );
}

export default ChatField
