import {
    HIDE_EDIT_FORM,
    SEND_MESSAGE,
    SET_DATA,
    SET_MESSAGES,
    SET_PREVIOUS_MESSAGE,
    SHOW_EDIT_FORM
} from "../actions/ActionTypes";

const initialState = {
    messages: [],
    prevMessage: undefined
}

export default (state = initialState, action: any) => {
    switch (action.type) {
        case SET_DATA:
            return {
                ...state,
                title: action.payload.title,
                isLoading: false
            }
        case SET_MESSAGES: {
            return {
                ...state,
                messages: action.payload.messages,
            }
        }
        case SEND_MESSAGE:
            return {
                ...state,
                messages: [...state.messages, action.payload.message],
                prevMessage: action.payload.message
            }
        case SET_PREVIOUS_MESSAGE:
            return {
                ...state,
                prevMessage: action.payload.prevMessage
            }
        case SHOW_EDIT_FORM:
            return {
                ...state,
                isShow: true,
                editMessage: action.payload.message
            }
        case HIDE_EDIT_FORM:
            return {
                ...state,
                isShow: false
            }
        default:
            return state;
    }
}
