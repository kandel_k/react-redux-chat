import {SET_CURR_USER, SET_USERS} from "../actions/ActionTypes";

const initialState = {
    messages: []
}

export default (state = initialState, action: any) => {
    switch (action.type) {
        case SET_USERS:
            return {
                ...state,
                users: action.payload.users
            }
        case SET_CURR_USER:
            return {
                ...state,
                currUser: action.payload.currUser
            }
        default:
            return state;
    }
}
