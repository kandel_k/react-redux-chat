import {applyMiddleware, combineReducers, createStore} from "redux";
import ChatReducer from "./reducers/ChatReducer";
import thunk from "redux-thunk";
import UserReducer from "./reducers/UserReducer";

const initialState = {};

const reducers = {
    chat: ChatReducer,
    users: UserReducer
}

const store = createStore(combineReducers(reducers), initialState, applyMiddleware(thunk));

export default store;
