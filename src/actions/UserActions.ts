import {SET_CURR_USER, SET_USERS} from "./ActionTypes";
import {UserInterface} from "../containers/Chat";

const setCurrUserAction = (data: any) => ({
    type: SET_CURR_USER,
    payload: data
})

const setUsersAction = (data: any) => ({
    type: SET_USERS,
    payload: data
})

export const setCurrUser = (currUser: UserInterface) => (dispatch: any) => {
    dispatch(setCurrUserAction({currUser: currUser}));
}

export const setUsers = (users: UserInterface[]) => (dispatch: any) => {
    dispatch(setUsersAction({users: users}));
}
