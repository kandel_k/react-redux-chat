export const SET_DATA = 'SET_DATA';
export const SET_MESSAGES = 'SET_MESSAGES';
export const SET_USERS = 'SET_USERS';
export const SET_CURR_USER = 'SET_CURR_USER';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const SHOW_EDIT_FORM = 'SHOW_EDIT_FORM';
export const HIDE_EDIT_FORM = 'HIDE_EDIT_FORM';
export const SET_PREVIOUS_MESSAGE = 'SET_PREVIOUS_MESSAGE';
