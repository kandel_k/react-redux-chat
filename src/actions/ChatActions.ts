import {
    HIDE_EDIT_FORM,
    SEND_MESSAGE,
    SET_DATA,
    SET_MESSAGES,
    SET_PREVIOUS_MESSAGE,
    SHOW_EDIT_FORM
} from "./ActionTypes";
import {MessageInterface, UserInterface} from "../containers/Chat";
import {LoadMessages} from "../services/MessageService";
import {setCurrUser, setUsers} from "./UserActions";

const setDataAction = (data: any) => ({
    type: SET_DATA,
    payload: data
});

const setMessagesAction = (data: any) => ({
    type: SET_MESSAGES,
    payload: data
});

const sendMessageAction = (data: any) => ({
    type: SEND_MESSAGE,
    payload: data
});

const showEditMessageAction = (data: any) => ({
    type: SHOW_EDIT_FORM,
    payload: data
});

const hideEditFormAction = () => ({
    type: HIDE_EDIT_FORM
});

const setPreviousMessageAction = (data: any) => ({
    type: SET_PREVIOUS_MESSAGE,
    payload: data
});

export const loadData = () => async (dispatch: any) => {
    const data: (UserInterface & MessageInterface)[] = await LoadMessages();

    dispatch(setMessages(extractMessages(data)));

    const userList = extractUsers(data);
    // I hardcode this user
    const currUser: UserInterface = {userId: 'This should be generated id or username', user: '', avatar: ''};
    userList.push(currUser);

    dispatch(setUsers(userList));
    dispatch(setCurrUser(currUser));
    dispatch(setDataAction({title: 'My chat'}));
};

const extractMessages = (data: MessageInterface[]) => {
    return data.map(el => {
        el.createdAt = new Date(el.createdAt);
        return el;
    });
}

const extractUsers = (data: UserInterface[]) => {
    return data.filter((v, i, a) => {
        return a.findIndex(user => user.userId === v.userId) === i
    });
}

export const setMessages = (messages: MessageInterface[]) => (dispatch: any) => {
    dispatch(setMessagesAction({messages: messages}));
}

export const sendMessage = (message: MessageInterface) => (dispatch: any) => {
    dispatch(sendMessageAction({message}))
}

export const deleteMessage = (id: string) => (dispatch: any, getState: any) => {
    const messages: MessageInterface[] = getState().chat.messages;
    const updated = messages.filter(message => message.id !== id);

    let prevMessage: any;
    for (let i = updated.length-1; i >= 0; i--) {
        if (updated[i].userId === getState().users.currUser.userId) {
            prevMessage = updated[i];
            break;
        }
    }

    dispatch(setMessagesAction({messages: updated}));
    dispatch(setPreviousMessageAction({prevMessage: prevMessage}));
};

export const likeMessage = (id: string) => (dispatch: any, getStore: any) => {
    const messages: MessageInterface[] = [...getStore().chat.messages];

    const message: any = messages.find(message => message.id === id);
    message.isLiked = !message.isLiked;

    dispatch(setMessagesAction({messages: messages}));
}

export const showEditMessageForm = (message: MessageInterface) => (dispatch: any) => {
    dispatch(showEditMessageAction({message: message}));
}

export const cancelEdit = () => (dispatch: any) => {
    dispatch(hideEditFormAction());
}

export const editMessage = (message: MessageInterface) => (dispatch: any, getState: any) => {
    const messages = [...getState().chat.messages];
    messages.map(mes => {
       if (mes.id === message.id) {
           mes.text = message.text;
           mes.editedAt = message.editedAt;
       }
       return mes;
    });
    dispatch(setMessagesAction({messages: messages}));
    dispatch(hideEditFormAction());
}
